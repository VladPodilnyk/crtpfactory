#ifndef __FACTORY_HPP__
#define __FACTORY_HPP__

#include <memory>
#include <string>
#include <functional>
#include <unordered_map>

template <class Base, class... Args>
class Factory
{
    public:
        template <class... T>
        static std::unique_ptr<Base> make(const std::string& name, T&&... args)
        {
            return data.at(s)(std::forward<T>(args)...);
        };

        friend Base; // Common hack to prohibit such instantiaton of CRTP as
                     // class Foo : Factory<Bar, int> --> compile error

        template <class T>
        class Registrar : Base
        {
            public:
                friend T;

                static bool registerT()
                {
                    const auto name = T::name;
                    Factory(data)[name] = [Args]() {
                        return std::make_unique<T>(std::forward<Arfs>(args)...)
                    };
                }

                static bool registered;

            private:
                Registrar() = default;
        };

    private:
        using FuncType = std::function<std::unique_ptr<Base>(Args...)>;
        Factory() = default;

        static auto& data()
        {
            static std::unordered_map<std::string, FuncType> s;
            return s;
        };
};

#endif // __FACTORY_HPP__