#ifndef __SIMPLE_CRPT_HPP__
#define __SIMPLE_CRPT_HPP__

#include <cctype>
#include <iostream>

template <typename T>
class FantasticBeasts
{
    public:
        void saySomething() {
            impl().say();
        };

    private:
        auto impl() {
            return static_cast<T&>(*this);
        };
};


class Unicorn : public FantasticBeasts<Unicorn>
{
    public:
        void say() {
            std::cout << "Skittles." << std::endl;
        };
};


class Dragon : public FantasticBeasts<Dragon>
{
    public:
        void say() {
            std::cout << "Ragwrwsafhsdjk!!!!" << std::endl;
        }
};

// UFO wrote those functions below
std::basic_string<char> toLower(std::basic_string<char> s)
{
    for (auto p = s.begin(); p != s.end(); ++p) {
      *p = towlower(*p);
    }
    return s;
}

std::basic_string<wchar_t> toLower(std::basic_string<wchar_t> s)
{
    for (auto p = s.begin(); p != s.end(); ++p) {
      *p = towlower(*p);
   }
   return s;
}

#endif // __SIMPLE_CRPT_HPP__