#include "factory.hpp"

int main()
{
    auto beast = FantasticBeasts<Dragon>{};
    beast.saySomething();
    return 0;
}